#!/bin/bash
# source this file anywhere you'd like python data structures.
# put ds_file_wrapper in ~/bin/
# This is more general and bash-focused than pandashells or similar
# Alternative design would be to just use jq and pure bash istead of python
# TODO docopts for better --set --get args

__datashell__() {
    if (($# > 1)); then
        if [ -z "${__datashell_store__[$1]}" ]; then
            __datashell_store__[$1]=$(./ds_fire_wrapper.py print_json "$2")
        else
            if [ "$2" = "mod" ]; then
                __datashell_store__[$1]=$(./ds_fire_wrapper.py evil "$2" "$3" "${__datashell_store__[$1]}")
            elif [ "$2" = "get" ]; then
                ./ds_fire_wrapper.py evil "$2" "$3" "${__datashell_store__[$1]}"
            fi
        fi
    elif (($# == 1)); then
        echo "${__datashell_store__[$1]}"
    fi
}

ds() {
    alias $1="__datashell__ $1"
}

shopt -s expand_aliases
declare -A __datashell_store__
