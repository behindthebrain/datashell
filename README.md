# datashell: A shell for your data
High level easy data structures in several shells: 
datashell brings any serializable data structures into the shell, from more convenient high level languages.

## Background
High level data structures in most shells are limited and painful to use.
Other projects skirt solutions to this problem:

* https://xon.sh/
  xonsh laboriously brings the shell into python.
* https://github.com/google/python-fire
  fire easily brings python functions into the shell.
* https://github.com/robdmc/pandashells
  brings pandas dataframe to the shell...
* https://github.com/jqlang/jq
  Processes json data as string IO
* https://github.com/sakishrist/bash-ext-arrays
  implements a mechanistically similar method to this project.
* https://stackoverflow.com/questions/11233825/multi-dimensional-arrays-in-bash
  discusses the need and hacks to accomplish the same goal as this project.
* https://stackoverflow.com/questions/6149679/multidimensional-associative-arrays-in-bash
  discusses the need and hacks to accomplish the same goal as this project.

Three projects generalize this even further:

* https://www.nushell.sh/
  structured data shell in Rust.
* https://elv.sh/
  structured data shell in Go.
* https://www.oilshell.org/
  structured data shell with a Python-centric approach

## Usage
In a shell script, for example: `example_usage.sh`

## Mechanisms
Each object is stored as a simple bash string in an associative array, in json format.
Thus, they can be accessed, manipulated, or processed directly with either:
1. arbitrary manual `jq` json processing, joined and fed back into a datashell object, or
2. datashell provides a python interface for modifying and accessing these simple json string objects as well.

## TOOD
- [ ] numpy and/or pandas objects?
- [ ] docopts for bash script
- [ ] function to evaluate any fire python functions that use the objects?
- [ ] function that takes a `jq` command and sets/gets the object
- [ ] could support any eval-based backend language, e.g., more than just python syntax.
- [ ] fish and zsh main drivers
