#!/bin/bash
# Dict and List examples for how to use datashell

source datashell.bash

## Example usage List
echo -e "\nDeclaring:"
ds a_list

echo -e "\nDefinining:"
a_list '["one", 1]'

echo -e "\nPrinting:"
a_list

echo -e "\nSetting values:"
a_list mod '.append(2)'
a_list mod '.append([1, 2])'

echo -e "\nGetting values:"
a_list get '[-1][1]'

echo -e "\nPretty printing with jq"
a_list | jq

## Example usage Dict
echo -e "\nDeclaring:"
ds big_dict

echo -e "\nDefinining:"
big_dict '{"name": "John", "age": 31, "city": "New York"}'

echo -e "\nPrinting:"
big_dict

echo -e "\nSetting values:"
big_dict mod '["age"]=21'
big_dict

echo -e "\nGetting values:"
big_dict get '["age"]'

echo -e "\nNesting complex variable references:"
big_dict mod '["name"]='"$(big_dict)"

echo -e "\nPretty printing with jq:"
big_dict | jq
