#!/usr/bin/python3

from typing import Any
import json
import fire


def print_json(old_var: Any) -> None:
    """For defining/initializing"""
    print(json.dumps(old_var))


def evil(setget: str, command: str, old_var: Any) -> None:
    """For setting/getting"""
    exec(f"global return_val; return_val = old_var{command}")
    if setget == "mod":
        print(json.dumps(old_var))
    elif setget == "get":
        print(json.dumps(return_val))


if __name__ == "__main__":
    fire.Fire()
